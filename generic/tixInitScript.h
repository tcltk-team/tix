/* 
 * tkInitScript.h --
 *
 *	This file contains Unix & Windows common init script
 *      It is not used on the Mac. (the mac init script is in tkMacInit.c)
 *
 * Copyright (c) 1997 Sun Microsystems, Inc.
 * Copyright (c) 2000-2001 Tix Project Group.
 *
 * See the file "license.terms" for information on usage and redistribution
 * of this file, and for a DISCLAIMER OF ALL WARRANTIES.
 *
 * $Id: tixInitScript.h,v 1.2 2004/03/28 02:44:57 hobbs Exp $
 */

/*
 * NOTE: the following comment was copied from TK 8.3.2 so it's probably
 *       correct, but refer to the tk_findLibrary docs for the definitive
 *       way of discovering Init.tcl
 *
 * [original TK comments follows]
 *
 * In order to find Init.tcl during initialization, the following script
 * is invoked by Tix_Init().  It looks in several different directories:
 *
 *	$tix_library		- can specify a primary location, if set
 *				  no other locations will be checked
 *
 *	$env(TIX_LIBRARY)	- highest priority so user can always override
 *				  the search path unless the application has
 *				  specified an exact directory above
 *
 *	$tcl_library/../tix$tix_version
 *				- look relative to init.tcl in an installed
 *				  lib directory (e.g. /usr/local)
 *
 *	<executable directory>/../lib/tix$tix_version
 *				- look for a lib/tix<ver> in a sibling of
 *				  the bin directory (e.g. /usr/local)
 *
 *	<executable directory>/../library
 *				- look in Tix build directory
 *
 *	<executable directory>/../../tix$tix_patchLevel/library
 *				- look for Tix build directory relative
 *				  to a parallel build directory
 *
 * The first directory on this path that contains a valid Init.tcl script
 * will be set as the value of tix_library.
 *
 * Note that this entire search mechanism can be bypassed by defining an
 * alternate tixInit procedure before calling Tix_Init().
 */

static char initScript[] = "if {[info proc tixInit]==\"\"} {\n\
  proc tixInit {} {\n\
    global tix_library tix_version tix_patchLevel\n\
    rename tixInit {}\n\
    tcl_findLibrary Tix $tix_version $tix_patchLevel Init.tcl TIX_LIBRARY tix_library\n\
  }\n\
}\n\
tixInit";

